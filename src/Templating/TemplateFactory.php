<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Templating;

class TemplateFactory implements \Nette\Application\UI\ITemplateFactory
{
	private \Nette\Application\UI\ITemplateFactory $templateFactory;
	private \Mdfx\PictureGenerator\Generator\PictureGeneratorFacade $pictureGenerator;

	public function __construct(
		\Nette\Application\UI\ITemplateFactory $templateFactory,
		\Mdfx\PictureGenerator\Generator\PictureGeneratorFacade $pictureGenerator
	) {
		$this->templateFactory = $templateFactory;
		$this->pictureGenerator = $pictureGenerator;
	}


	public function createTemplate(?\Nette\Application\UI\Control $control = NULL): \Nette\Application\UI\ITemplate
	{
		/** @var \Nette\Bridges\ApplicationLatte\Template $template */
		$template = $this->templateFactory->createTemplate($control);

		$template->setParameters([
			'pictureGenerator' => $this->pictureGenerator,
		]);

		return $template;
	}
}
