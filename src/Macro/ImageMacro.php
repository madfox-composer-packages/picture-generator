<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Macro;

class ImageMacro extends \Latte\Macros\MacroSet
{

	private const DEFAULT_TEMPLATE_FILE_LAZY = __DIR__ . '/templates/default.latte';
	private const DEFAULT_TEMPLATE_FILE_NORMAL = __DIR__ . '/templates/no-lazy.latte';

	public function __construct(
		\Latte\Compiler $compiler
	)
	{
		parent::__construct($compiler);
	}


	public static function install(\Latte\Compiler $compiler): void
	{
		$set = new static($compiler);
		$set->addMacro('image', [$set, 'image']);
		$set->addMacro('imageLink', [$set, 'imageLink']);
	}


	public function image(\Latte\MacroNode $node, \Latte\PhpWriter $writer): string
	{
		$macro = "echo \Mdfx\PictureGenerator\Macro\ImageMacro::imageHelper(%node.word, %node.array, \$pictureGenerator)";

		return $writer->write($macro);
	}

	public function imageLink(\Latte\MacroNode $node, \Latte\PhpWriter $writer): string
	{
		$macro = "echo \Mdfx\PictureGenerator\Macro\ImageMacro::imageLinkHelper(%node.word, %node.array, \$pictureGenerator)";

		return $writer->write($macro);
	}

	public static function imageLinkHelper(
		$image,
		array $options,
		\Mdfx\PictureGenerator\Generator\PictureGeneratorFacade $pictureGeneratorFacade
	): string
	{
		if (\is_string($image)) {
			$image = self::createFromString($image);
		}

		/** @var string|NULL $size */
		$size = $options['size'] ?? NULL;
		if ($size === NULL) {
			throw new \UnexpectedValueException('Image macro must have its size name set.');
		}

		$key = $image ? $image->getKey() : $pictureGeneratorFacade->getPlaceholderKey($size);

		return $fallbackUrl = $pictureGeneratorFacade->generateFallbackUrl($key, $size);
	}

	public static function imageHelper(
		$image,
		array $options,
		\Mdfx\PictureGenerator\Generator\PictureGeneratorFacade $pictureGeneratorFacade
	): string
	{
		if (\is_string($image)) {
			$image = self::createFromString($image);
		}

		$class = $options['class'] ?? 'o-image';
		$alt = $options['alt'] ?? '';
		$lazyLoad = (bool) ($options['lazy'] ?? TRUE);

		/** @var string|NULL $size */
		$size = $options['size'] ?? NULL;
		if ($size === NULL) {
			throw new \Mdfx\PictureGenerator\Exception\PictureKeyNotSetException(
				'Image macro must have its size name set.'
			);
		}

		$key = $image ? $image->getKey() : $pictureGeneratorFacade->getPlaceholderKey($size);
		$sources = $pictureGeneratorFacade->generateSources($key, $size);
		$fallbackUrl = $pictureGeneratorFacade->generateFallbackUrl($key, $size);

		$template = new \Latte\Engine;

		$params = [
			'class' => $class,
			'sources' => $sources,
			'fallbackUrl' => $fallbackUrl,
			'alt' => $alt,
		] + $options;

		if ($lazyLoad) {
			$thumbnailSources = $pictureGeneratorFacade->generateThumbnailSources($key, $size);
			$thumbnailFallbackUrl = $pictureGeneratorFacade->generateThumbnailFallbackUrl($key, $size);

			$params += [
				'thumbnailSources' => $thumbnailSources,
				'thumbnailFallbackUrl' => $thumbnailFallbackUrl,
			];

			$templateFile = $pictureGeneratorFacade->getTemplateFile() ?? self::DEFAULT_TEMPLATE_FILE_LAZY;
		} else {
			$templateFile = $pictureGeneratorFacade->getTemplateFileNoLazy() ?? self::DEFAULT_TEMPLATE_FILE_NORMAL;
		}

		return $template->renderToString(
			$templateFile,
			$params
		);
	}

	private static function createFromString(string $key): \Mdfx\PictureGenerator\Contract\ImageInterface
	{
		return new \Mdfx\PictureGenerator\StaticImage\StaticImage($key);
	}

}
