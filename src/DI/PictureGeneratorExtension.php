<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\DI;

class PictureGeneratorExtension extends \Nette\DI\CompilerExtension
{

	public function loadConfiguration()
	{
		$config = (array) $this->getConfig();

		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('configuration'))
			->setFactory(\Mdfx\PictureGenerator\Configuration\Configuration::class)
			->setArguments([
				$config['baseUrl'],
				$config['basePlaceholderKey'],
				$config['templateFile'],
				$config['templateFileNoLazy'],
				$config['defaultType'],
				$config['defaultQuality'],
				$config['avif'],
				(array) $config['keys'],
			])
		;

		$builder->addDefinition($this->prefix('staticImageGetter'))
			->setFactory(\Mdfx\PictureGenerator\StaticImage\StaticImageGetter::class)
			->setArguments([
				(array) $config['static'],
			])
		;

		$builder->addDefinition($this->prefix('configuration.marshaller'))
			->setFactory(\Mdfx\PictureGenerator\Configuration\Marshaller\ConfigurationMarshaller::class)
		;

		$builder->addDefinition($this->prefix('pictureGeneratorFacade'))
			->setFactory(\Mdfx\PictureGenerator\Generator\PictureGeneratorFacade::class)
		;

		$builder->addDefinition($this->prefix('sourceGenerator'))
			->setFactory(\Mdfx\PictureGenerator\Generator\SourceGenerator::class)
		;

		$builder->addDefinition($this->prefix('thumbnailGenerator'))
			->setFactory(\Mdfx\PictureGenerator\Generator\ThumbnailGenerator::class)
		;

		$builder->addDefinition($this->prefix('urlGenerator'))
			->setFactory(\Mdfx\PictureGenerator\Generator\UrlGenerator::class)
		;

		$builder->addDefinition($this->prefix('templateFactory'))
			->setFactory(\Mdfx\PictureGenerator\Templating\TemplateFactory::class)
			->setArguments([
				'@latte.templateFactory',
			])
		;
	}

	public function beforeCompile()
	{
		parent::beforeCompile();

		$builder = $this->getContainerBuilder();

		/** @var \Nette\DI\Definitions\FactoryDefinition $latteFactoryDefinition */
		$latteFactoryDefinition = $builder->getDefinition('latte.latteFactory');
		$latteFactoryDefinition->getResultDefinition()
			->addSetup('?->onCompile[] = function($engine) { Mdfx\PictureGenerator\Macro\ImageMacro::install($engine->getCompiler()); }', ['@self']);

		$builder->getDefinition('latte.templateFactory')
			->setAutowired(FALSE)
		;
	}


	public function getConfigSchema(): \Nette\Schema\Schema
	{
		return \Nette\Schema\Expect::structure([
			'baseUrl' => \Nette\Schema\Expect::string()->required(),
			'basePlaceholderKey' => \Nette\Schema\Expect::string()->nullable(),
			'templateFile' => \Nette\Schema\Expect::string()->nullable(),
			'templateFileNoLazy' => \Nette\Schema\Expect::string()->nullable(),
			'defaultType' => \Nette\Schema\Expect::string('jpg'),
			'defaultQuality' => \Nette\Schema\Expect::int(80),
			'avif' => \Nette\Schema\Expect::bool(FALSE),
			'keys' => \Nette\Schema\Expect::arrayOf(
				\Nette\Schema\Expect::structure([
					'avif' => \Nette\Schema\Expect::bool()->nullable(),
					'type' => \Nette\Schema\Expect::string()->nullable(),
					'quality' => \Nette\Schema\Expect::int()->nullable(),
					'placeholderKey' => \Nette\Schema\Expect::string()->nullable(),
					'fallback' => \Nette\Schema\Expect::structure([
						'width' => \Nette\Schema\Expect::int()->nullable(),
						'height' => \Nette\Schema\Expect::int()->nullable(),
					]),
					'sources' => \Nette\Schema\Expect::arrayOf(
						\Nette\Schema\Expect::structure([
							'dpr' => \Nette\Schema\Expect::arrayOf(\Nette\Schema\Expect::float())->default([1.0]),
							'width' => \Nette\Schema\Expect::int()->nullable(),
							'height' => \Nette\Schema\Expect::int()->nullable(),
							'media' => \Nette\Schema\Expect::structure([
								'min' => \Nette\Schema\Expect::int()->nullable(),
								'max' => \Nette\Schema\Expect::int()->nullable(),
								'custom' => \Nette\Schema\Expect::string()->nullable(),
							]),
						]),
					),
				]),
				),
			'static' => \Nette\Schema\Expect::arrayOf(
				\Nette\Schema\Expect::structure([
					'key' => \Nette\Schema\Expect::string()->required(),
				]),
			),
		]);
	}

}
