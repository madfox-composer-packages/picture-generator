<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Configuration;

class Types
{

	public const TYPE_WEBP = 'webp';

	public const TYPE_AVIF = 'avif';

}
