<?php declare(strict_types=1);

namespace Mdfx\PictureGenerator\Configuration\Marshaller;

class ConfigurationMarshaller
{

	/**
	 * @return \Mdfx\PictureGenerator\Configuration\DTO\Picture[]
	 */
	public function marshallKeys(
		array $keys,
		string $defaultType,
		int $defaultQuality,
		bool $useAvif,
	): array
	{
		$keys = \array_map(static fn(object $key): array => (array) $key, $keys);

		return \array_map(
			fn (array $key): \Mdfx\PictureGenerator\Configuration\DTO\Picture => $this->marshallPicture(
				$key['type'] ?? $defaultType,
				$key['quality'] ?? $defaultQuality,
				$key['fallback'],
				$key['sources'],
				$key['placeholderKey'],
				$key['avif'] ?? $useAvif,
			),
			$keys
		);
	}

	private function marshallPicture(
		string $type,
		int $quality,
		object $fallbackDimensions,
		array $sources,
		?string $placeHolderKey,
		bool $useAvif,
	): \Mdfx\PictureGenerator\Configuration\DTO\Picture
	{
		$sources = \array_map(static fn(object $key): array => (array) $key, $sources);

		$sources = \array_map(
			fn (array $source): \Mdfx\PictureGenerator\Configuration\DTO\Source => $this->marshallSource(
				\array_unique($source['dpr']),
				$source['width'],
				$source['height'],
				$source['media'],
			),
			$sources
		);

		$fallbackDimensions = (array) $fallbackDimensions;

		return new \Mdfx\PictureGenerator\Configuration\DTO\Picture(
			$type,
			$quality,
			new \Mdfx\PictureGenerator\Configuration\DTO\Dimensions(
				$fallbackDimensions['width'],
				$fallbackDimensions['height'],
			),
			$sources,
			$placeHolderKey,
			$useAvif
		);
	}

	private function marshallSource(
		array $dpr,
		?int $width,
		?int $height,
		object $media
	): \Mdfx\PictureGenerator\Configuration\DTO\Source
	{
		$media = (array) $media;

		return new \Mdfx\PictureGenerator\Configuration\DTO\Source(
			\array_unique($dpr),
			new \Mdfx\PictureGenerator\Configuration\DTO\Dimensions($width, $height),
			new \Mdfx\PictureGenerator\Configuration\DTO\Media(
				$media['min'],
				$media['max'],
				$media['custom'],
			)
		);
	}
}
