<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Configuration\DTO;

class Dimensions
{
	private ?int $width;
	private ?int $height;

	public function __construct(
		?int $width,
		?int $height
	)
	{
		if ($width === NULL && $height === NULL) {
			throw new \Mdfx\PictureGenerator\Exception\IncorrectDimensionsException(
				"At least either height or width must be set.",
			);
		}

		$this->width = $width;
		$this->height = $height;
	}

	public function getWidth(): ?int
	{
		return $this->width;
	}

	public function getHeight(): ?int
	{
		return $this->height;
	}

	public static function multiplyDimensionsByDpr(Dimensions $dimensions, float $dpr): Dimensions
	{
		return new Dimensions(
			$dimensions->getWidth() ? (int) ($dimensions->getWidth() * $dpr) : NULL,
			$dimensions->getHeight() ? (int) ($dimensions->getHeight() * $dpr) : NULL,
		);
	}

}
