<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Configuration\DTO;

class Picture
{

	/**
	 * @param \Mdfx\PictureGenerator\Configuration\DTO\Source[] $sources
	 */
	public function __construct(
		private string $type,
		private int $quality,
		private \Mdfx\PictureGenerator\Configuration\DTO\Dimensions $fallback,
		private array $sources,
		private ?string $placeHolderKey,
		private bool $avifAllowed,
	) {}

	public function getType(): string
	{
		return $this->type;
	}

	public function getQuality(): int
	{
		return $this->quality;
	}

	public function getFallback(): \Mdfx\PictureGenerator\Configuration\DTO\Dimensions
	{
		return $this->fallback;
	}

	/**
	 * @return \Mdfx\PictureGenerator\Configuration\DTO\Source[]
	 */
	public function getSources(): array
	{
		return $this->sources;
	}

	public function getPlaceHolderKey(): ?string
	{
		return $this->placeHolderKey;
	}

	public function isAvifAllowed(): bool
	{
		return $this->avifAllowed;
	}

}
