<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Configuration\DTO;

class Source
{

	private array $dpr;
	private Dimensions $dimensions;
	private ?Media $media;

	public function __construct(
		array $dpr,
		Dimensions $dimensions,
		?Media $media
	)
	{

		$this->dpr = $dpr;
		$this->dimensions = $dimensions;
		$this->media = $media;
	}

	public function getDpr(): array
	{
		return $this->dpr;
	}

	public function getDimensions(): \Mdfx\PictureGenerator\Configuration\DTO\Dimensions
	{
		return $this->dimensions;
	}

	public function getMedia(): ?\Mdfx\PictureGenerator\Configuration\DTO\Media
	{
		return $this->media;
	}
}
