<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Configuration\DTO;

class Media
{
	private ?int $min;
	private ?int $max;
	private ?string $custom;

	public function __construct(
		?int $min,
		?int $max,
		?string $custom
	)
	{
		$this->max = $max;
		$this->min = $min;
		$this->custom = $custom;
	}

	public function getMin(): ?int
	{
		return $this->min;
	}

	public function getMax(): ?int
	{
		return $this->max;
	}

	public function getCustom(): ?string
	{
		return $this->custom;
	}

}
