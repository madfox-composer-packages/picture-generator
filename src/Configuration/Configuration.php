<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Configuration;

class Configuration
{

	/**
	 * @var \Mdfx\PictureGenerator\Configuration\DTO\Picture[]
	 */
	private array $pictures;

	public function __construct(
		private string $baseUrl,
		private ?string $basePlaceholderKey,
		private ?string $templateFile,
		private ?string $templateFileNoLazy,
		private string $defaultType,
		private int $defaultQuality,
		private bool $avif,
		array $pictures,
		\Mdfx\PictureGenerator\Configuration\Marshaller\ConfigurationMarshaller $configurationMarshaller,
	)
	{
		$this->pictures = $configurationMarshaller->marshallKeys(
			$pictures,
			$defaultType,
			$defaultQuality,
			$avif,
		);
	}


	public function getBaseUrl(): string
	{
		return $this->baseUrl;
	}


	public function getPicture(string $key): \Mdfx\PictureGenerator\Configuration\DTO\Picture
	{
		$picture = $this->pictures[$key] ?? NULL;

		if ($picture === NULL) {
			throw new \Mdfx\PictureGenerator\Exception\UndefinedPictureKeyException("Key $key is not defined.");
		}

		return $picture;
	}

	public function getBasePlaceholderKey(): ?string
	{
		return $this->basePlaceholderKey;
	}

	public function getTemplateFile(): ?string
	{
		return $this->templateFile;
	}

	public function getTemplateFileNoLazy(): ?string
	{
		return $this->templateFileNoLazy;
	}

}
