<?php declare(strict_types=1);

namespace Mdfx\PictureGenerator\StaticImage;

class StaticImage implements \Mdfx\PictureGenerator\Contract\ImageInterface
{

	private string $key;

	public function __construct(
		string $key
	)
	{
		$this->key = $key;
	}

	public function getKey(): string
	{
		return $this->key;
	}

}
