<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\StaticImage;

class StaticImageGetter implements \Mdfx\PictureGenerator\Contract\StaticImageGetterInterface
{

	/**
	 * @var \Mdfx\PictureGenerator\StaticImage\StaticImage[]
	 */
	private array $images;

	public function __construct(
		array $staticKeys
	)
	{
		$this->images = $this->configureStatic($staticKeys);
	}

	private function configureStatic(array $static): array
	{
		$data = [];
		foreach ($static as $key => $image) {
			$data[$key] = new \Mdfx\PictureGenerator\StaticImage\StaticImage(
				$image->key,
			);
		}

		return $data;
	}

	public function getImage(string $key): \Mdfx\PictureGenerator\Contract\ImageInterface
	{
		$image = $this->images[$key] ?? NULL;

		if ($image === NULL) {
			throw new \Mdfx\PictureGenerator\Exception\UndefinedStaticKeyException("Key $key is not set.");
		}

		return $image;
	}

}
