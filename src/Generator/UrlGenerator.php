<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Generator;

class UrlGenerator implements \Mdfx\PictureGenerator\Contract\UrlGeneratorInterface
{

	public function __construct(
		private \Mdfx\PictureGenerator\Configuration\Configuration $configuration
	) { }

	public function generateUrl(
		string $key,
		\Mdfx\PictureGenerator\Configuration\DTO\Dimensions $dimensions,
		int $quality,
		string $type
	): string
	{
		$params = \http_build_query($this->getUrlParameters($dimensions, $quality, $type));

		$baseUrl = $this->configuration->getBaseUrl();

		return "$baseUrl/$key?$params";
	}


	private function getUrlParameters(
		\Mdfx\PictureGenerator\Configuration\DTO\Dimensions $dimensions,
		int $quality,
		string $type
	): array {
		$params = [
			'quality' => $quality,
			'type' => $type,
		];

		if ($dimensions->getHeight()) {
			$params['height'] = $dimensions->getHeight();
		}

		if ($dimensions->getWidth()) {
			$params['width'] = $dimensions->getWidth();
		}

		return $params;
	}

}
