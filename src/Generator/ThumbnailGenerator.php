<?php declare(strict_types=1);

namespace Mdfx\PictureGenerator\Generator;

class ThumbnailGenerator implements \Mdfx\PictureGenerator\Contract\ThumbnailGeneratorInterface
{
	private const DEFAULT_DIMENSION = 200;
	private const DEFAULT_QUALITY = 20;
	private const DEFAULT_TYPE = 'jpg';

	public function __construct(
		private SourceGenerator $sourceGenerator,
		private \Mdfx\PictureGenerator\Contract\UrlGeneratorInterface $urlGenerator
	) { }

	public function generateThumbnailSources(
		string $key,
		\Mdfx\PictureGenerator\Configuration\DTO\Picture $picture
	): array
	{
		$dimensions = $this->getThumbnailDimensions($picture);

		$source = new \Mdfx\PictureGenerator\Configuration\DTO\Source([1.0], $dimensions, NULL);
		$picture = new \Mdfx\PictureGenerator\Configuration\DTO\Picture(
			self::DEFAULT_TYPE,
			self::DEFAULT_QUALITY,
			$dimensions,
			[$source],
			NULL,
			$picture->isAvifAllowed(),
		);

		$sources = [
			$this->sourceGenerator->generateSource($key, $picture, $source, \Mdfx\PictureGenerator\Configuration\Types::TYPE_WEBP),
			$this->sourceGenerator->generateSource($key, $picture, $source),
		];

		if ($picture->isAvifAllowed()) {
			$sources = [
				$this->sourceGenerator->generateSource($key, $picture, $source, \Mdfx\PictureGenerator\Configuration\Types::TYPE_AVIF),
				...$sources,
			];
		}

		return $sources;
	}


	public function generateThumbnailFallbackImage(
		string $key,
		\Mdfx\PictureGenerator\Configuration\DTO\Picture $picture
	): string
	{
		$thumbnailDimensions = $this->getThumbnailDimensions($picture);

		return $this->urlGenerator->generateUrl(
			$key,
			$thumbnailDimensions,
			self::DEFAULT_QUALITY,
			self::DEFAULT_TYPE,
		);
	}


	private function getThumbnailDimensions(
		\Mdfx\PictureGenerator\Configuration\DTO\Picture $picture
	): \Mdfx\PictureGenerator\Configuration\DTO\Dimensions
	{
		$fallbackDimensions = $picture->getFallback();
		$width = self::DEFAULT_DIMENSION;
		$height = NULL;

		if ($fallbackDimensions->getWidth() && $fallbackDimensions->getHeight()) {
			$height = (int) ($width * ($fallbackDimensions->getHeight() / $fallbackDimensions->getWidth()));
		} else if ($fallbackDimensions->getWidth() === NULL && $fallbackDimensions->getHeight()) {
			$height = self::DEFAULT_DIMENSION;
			$width = NULL;
		}

		return new \Mdfx\PictureGenerator\Configuration\DTO\Dimensions($width, $height);
	}
}
