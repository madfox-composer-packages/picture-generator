<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Generator;

class PictureGeneratorFacade
{
	private \Mdfx\PictureGenerator\Configuration\Configuration $configuration;
	private \Mdfx\PictureGenerator\Contract\UrlGeneratorInterface $urlGenerator;
	private \Mdfx\PictureGenerator\Contract\ThumbnailGeneratorInterface $thumbnailGenerator;
	private SourceGenerator $sourceGenerator;

	public function __construct(
		\Mdfx\PictureGenerator\Configuration\Configuration $configuration,
		\Mdfx\PictureGenerator\Contract\UrlGeneratorInterface $urlGenerator,
		\Mdfx\PictureGenerator\Contract\ThumbnailGeneratorInterface $thumbnailGenerator,
		SourceGenerator $sourceGenerator
	)
	{
		$this->configuration = $configuration;
		$this->urlGenerator = $urlGenerator;
		$this->thumbnailGenerator = $thumbnailGenerator;
		$this->sourceGenerator = $sourceGenerator;
	}

	public function generateFallbackUrl(string $key, string $size): string
	{
		$picture = $this->configuration->getPicture($size);

		return $this->urlGenerator->generateUrl(
			$key,
			$picture->getFallback(),
			$picture->getQuality(),
			$picture->getType()
		);
	}

	public function generateThumbnailFallbackUrl(string $key, string $size): string
	{
		$picture = $this->configuration->getPicture($size);

		return $this->thumbnailGenerator->generateThumbnailFallbackImage($key, $picture);
	}

	public function generateSources(string $key, string $size): array
	{
		$picture = $this->configuration->getPicture($size);

		return $this->sourceGenerator->generateSources($key, $picture);
	}

	public function generateThumbnailSources(string $key, string $size): array
	{
		$picture = $this->configuration->getPicture($size);

		return $this->thumbnailGenerator->generateThumbnailSources($key, $picture);
	}

	public function getPlaceholderKey(string $size): ?string
	{
		$picture = $this->configuration->getPicture($size);

		return $picture->getPlaceHolderKey() ?? $this->configuration->getBasePlaceholderKey();
	}

	public function getTemplateFile(): ?string
	{
		return $this->configuration->getTemplateFile();
	}

	public function getTemplateFileNoLazy(): ? string
	{
		return $this->configuration->getTemplateFileNoLazy();
	}
}
