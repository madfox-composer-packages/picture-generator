<?php declare(strict_types=1);

namespace Mdfx\PictureGenerator\Generator\DTO;

class Source
{

	private string $srcset;
	private ?string $media;
	private ?string $type;

	public function __construct(
		string $srcset,
		?string $media,
		?string $type
	)
	{
		$this->srcset = $srcset;
		$this->media = $media;
		$this->type = $type;
	}

	public function getSrcset(): string
	{
		return $this->srcset;
	}

	public function getMedia(): ?string
	{
		return $this->media;
	}

	public function getType(): ?string
	{
		return $this->type;
	}

}
