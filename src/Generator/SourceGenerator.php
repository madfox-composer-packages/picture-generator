<?php declare(strict_types=1);

namespace Mdfx\PictureGenerator\Generator;

class SourceGenerator
{

	public function __construct(
		private \Mdfx\PictureGenerator\Contract\UrlGeneratorInterface $urlGenerator
	) {}

	public function generateSources(string $key, \Mdfx\PictureGenerator\Configuration\DTO\Picture $picture): array
	{
		if ( ! \count($picture->getSources())) {
			return $this->generateFallbackSources($key, $picture);
		}

		$sources = [];

		if ($picture->isAvifAllowed()) {
			$avif = \array_map(fn (\Mdfx\PictureGenerator\Configuration\DTO\Source $source): \Mdfx\PictureGenerator\Generator\DTO\Source =>
			$this->generateSource($key, $picture, $source, \Mdfx\PictureGenerator\Configuration\Types::TYPE_AVIF), $picture->getSources());
			$sources += $avif;
		}

		$webp = \array_map(fn (\Mdfx\PictureGenerator\Configuration\DTO\Source $source): \Mdfx\PictureGenerator\Generator\DTO\Source =>
		$this->generateSource($key, $picture, $source, \Mdfx\PictureGenerator\Configuration\Types::TYPE_WEBP), $picture->getSources());
		$sources += $webp;

		$classic = \array_map(fn (\Mdfx\PictureGenerator\Configuration\DTO\Source $source): \Mdfx\PictureGenerator\Generator\DTO\Source =>
		$this->generateSource($key, $picture, $source), $picture->getSources());
		$sources += $classic;

		return $sources;
	}

	protected function generateFallbackSources(string $key, \Mdfx\PictureGenerator\Configuration\DTO\Picture $picture): array
	{
		$dimensions = new \Mdfx\PictureGenerator\Configuration\DTO\Dimensions($picture->getFallback()->getWidth(), $picture->getFallback()->getHeight());
		$source = new \Mdfx\PictureGenerator\Configuration\DTO\Source([1.0, 2.0], $dimensions, NULL);

		$sources = [
			$this->generateSource($key, $picture, $source, \Mdfx\PictureGenerator\Configuration\Types::TYPE_WEBP),
			$this->generateSource($key, $picture, $source),
		];

		if ($picture->isAvifAllowed()) {
			$sources = [
				$this->generateSource($key, $picture, $source, \Mdfx\PictureGenerator\Configuration\Types::TYPE_AVIF),
				...$sources,
			];
		}

		return $sources;
	}

	public function generateSource(
		string $key,
		\Mdfx\PictureGenerator\Configuration\DTO\Picture $picture,
		\Mdfx\PictureGenerator\Configuration\DTO\Source $source,
		?string $type = NULL
	): \Mdfx\PictureGenerator\Generator\DTO\Source
	{
		return new \Mdfx\PictureGenerator\Generator\DTO\Source(
			$this->generateSrcset($key, $picture, $source, $type),
			$source->getMedia() ? $this->generateMedia($source->getMedia()) : NULL,
			$this->generateType($type),
		);
	}

	private function generateSrcset(
		string $key,
		\Mdfx\PictureGenerator\Configuration\DTO\Picture $picture,
		\Mdfx\PictureGenerator\Configuration\DTO\Source $source,
		?string $type = NULL
	): string
	{
		$srcset = [];
		foreach ($source->getDpr() as $dpr) {
			$url = $this->urlGenerator->generateUrl(
				$key,
				\Mdfx\PictureGenerator\Configuration\DTO\Dimensions::multiplyDimensionsByDpr($source->getDimensions(), $dpr),
				$picture->getQuality(),
				$type ?? $picture->getType(),
			);

			$url .= " {$dpr}x";

			$srcset[] = $url;
		}

		return \implode(', ', $srcset);
	}

	private function generateType(?string $type = NULL): ?string
	{
		return $type ? "image/$type" : NULL;
	}

	private function generateMedia(\Mdfx\PictureGenerator\Configuration\DTO\Media $media): ?string
	{
		if ($media->getCustom()) {
			return $media->getCustom();
		} else if ($media->getMax() && $media->getMin()) {
			return "(min-width:{$media->getMin()}px) and (max-width: {$media->getMax()}px)";
		} else if ($media->getMax()) {
			return "(max-width: {$media->getMax()}px)";
		} else if ($media->getMin()) {
			return "(min-width: {$media->getMin()}px)";
		}

		return NULL;
	}
}
