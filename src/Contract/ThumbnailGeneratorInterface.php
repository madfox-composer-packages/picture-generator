<?php declare(strict_types=1);

namespace Mdfx\PictureGenerator\Contract;

interface ThumbnailGeneratorInterface
{

	public function generateThumbnailSources(
		string $key,
		\Mdfx\PictureGenerator\Configuration\DTO\Picture $picture
	): array;

	public function generateThumbnailFallbackImage(
		string $key,
		\Mdfx\PictureGenerator\Configuration\DTO\Picture $picture
	): string;

}
