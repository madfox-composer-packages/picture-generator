<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Contract;

interface UrlGeneratorInterface
{

	public function generateUrl(
		string $key,
		\Mdfx\PictureGenerator\Configuration\DTO\Dimensions $dimensions,
		int $quality,
		string $type
	): string;

}
