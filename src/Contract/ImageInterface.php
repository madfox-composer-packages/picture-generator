<?php declare(strict_types=1);

namespace Mdfx\PictureGenerator\Contract;

interface ImageInterface
{

	public function getKey(): string;

}
