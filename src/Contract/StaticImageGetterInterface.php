<?php declare(strict_types=1);

namespace Mdfx\PictureGenerator\Contract;

interface StaticImageGetterInterface
{

	public function getImage(string $key): ImageInterface;

}
