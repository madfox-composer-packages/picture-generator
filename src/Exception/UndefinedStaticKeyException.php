<?php declare(strict_types = 1);

namespace Mdfx\PictureGenerator\Exception;


class UndefinedStaticKeyException extends \RuntimeException
{

}
